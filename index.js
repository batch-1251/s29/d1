let express=require("express");
const PORT=4000;

let app=express();
	//app is now our server


//middlewares-setup for allowing the server to handle data from request(client)
app.use(express.json());
	//allow your app to read json data
app.use(express.urlencoded({extended:true}));
	//allow your app to read data from the forms

//routes
app.get("/", (req, res)=>res.send("Hello World"));

app.get("/hello", (req, res)=>res.send("Hello"));

app.post("/greeting",(req, res)=>{
	console.log(req.body);
	
	res.send(`Hello there,${req.body.firstname}!`)
});


app.listen(PORT,()=>{
	console.log(`Server running at port${PORT}`);
});